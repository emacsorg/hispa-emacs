provider "aws" {
  profile = "hispa-emacs"
  region = "eu-central-1"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "jitsi" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "m5.large"
  key_name      = "hispa-emacs"
  vpc_security_group_ids = [aws_security_group.jitsi.id]

  root_block_device {
    volume_size = 100
  }

  tags = {
    Name = "jitsi"
  }

  lifecycle {
    ignore_changes = [ami, instance_type]
  }
}

resource "aws_eip" "jitsi" {
  instance = aws_instance.jitsi.id
  vpc      = true
}

resource "aws_security_group" "jitsi" {
  name   = "jitsi-security-group"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 4443
    to_port     = 4443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 10000
    to_port     = 20000
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_key_pair" "hispa-emacs" {
  key_name = "hispa-emacs"
  public_key = "ssh-rsa XXXX hispa-emacs"
}
