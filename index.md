---
title: (hispa-emacs)
layout: default
---
{::options parse_block_html="true" /}

<section id="above-fold"><div class="row"><div class="large-12 columns intro-info">

{:#emacs-berlin-lockup width="300px"}
![emacs logo](img/hispa-emacs.org.png)

</div></div></section>

<section id="below-fold"><div class="row"><div class="medium-8 columns">

---

# 2025 : 1a Hispa-Emacs Conf. - grabaciones :

---
Ya _estamos rumbo a la Conf. de 2026._ Entretanto, nos seguiremos reuniendo online mensualmente.

> ¿Qué pasó?: 2 Jornadas de Microtalleres online de 15/30 min. ~ HORARIOS referidos en base a CET : p.ej.: 18h en Madrid = 14h en Buenos Aires

> ¿ Dónde fue ? : [acá nos encuentras, en la Sala Video](https://videoconferencia.unizar.es/rooms/rq3-38j-vfb-skz/join) ~~~ [cortesía de la Oficina del Software Libre de la Universidad de Zaragoza](https://osluz.unizar.es/)

> ¿Quedaron[ grabadas las videosesiones ? SI >>> aquí (click) las ves en diferido.](https://fediverse.tv/a/emacs_org/video-channels)
        
## **17 de Enero** - Día 1 :
---


### 18h CET "Apertura y bienvenida ~ la Comunidad Hispa-Emacs"


### 18:15 CET "La _Comunidad_ GNU Emacs - retrospectiva : Orígenes - Pasado, Presente y Futuro ?"  (by Servio)


### 18:45 CET "Distribución** Emacs _Lloica_ : Desde el sur del mundo una configuración especial para programadores y proyectos pedagógicos.. " (by David Pineiden)

  [https://gitlab.com/pineiden/lloica-emacs](https://gitlab.com/pineiden/lloica-emacs)


### 19:30 CET "Construyendo el _website Hispa-Emacs.org_ enteramente desde Emacs " (by fenix)

    ... con (''org-static-blog) ~ Elisp 




## **18 de Enero**  - Día 2 :
---


### 19:00 CET "_Mesa redonda_ ~ Comunidad Hispa-Emacs: propuestas, horizonte y perspectivas rumbo a Enero 2026" - debate


### 19:30 CET "Emacs y _twtxt_, la red social en texto plano, descentralizada y minimalista" (by Andros)

[https://programadorwebvalencia.com/twtxt-la-red-social-en-texto-plano-descentralizada-y-minimalista/](https://programadorwebvalencia.com/twtxt-la-red-social-en-texto-plano-descentralizada-y-minimalista/)


### 20:00 CET "Emacs Hyperdrive"  ~  EN | ES (traducción simultánea ) (by Joseph Turner)

[https://emacsconf.org/2024/talks/hyperdrive/](https://emacsconf.org/2024/talks/hyperdrive/)
[Hyperdrive P2P ~ Manual](https://ushin.org/hyperdrive/hyperdrive-manual.html)
 

### 20:45 Cloenda y despedida 


---


# 2024 :

---

## Pasado encuentro/taller online celebrado (video / grabación):  "Integra cualquier linter a emacs con flymake"
### ¿Cuándo fue? ¿ Quedó grabado y publicado ?

Erick nos hablló en torno a la posibilidad de  "Integrar cualquier linter [Emacs con flymake"](https://www.emacswiki.org/emacs/FlyMake) 

Según [Wikipedia, un Linter](https://es.wikipedia.org/wiki/Lint) "es una herramienta de programación; originalmente lint era el nombre de una herramienta de programación utilizada para detectar código sospechoso, confuso o incompatible entre distintas arquitecturas en programas escritos en C; es decir, errores de programación que escapan al habitual análisis sintáctico que hace el compilador. En la actualidad, se utiliza este término para designar a herramientas que realizan estas tareas de comprobación en cualquier lenguaje de programación. Las herramientas de tipo lint generalmente funcionan realizando un análisis estático del código fuente. "

> NOTA:  Los encuentros virtuales se celebran en la [sala de video BigBlueButton cortesía de la Oficina del Software Libre de la Universidad de Zaragoza](https://osluz.unizar.es/)

El evento tuvo lugar en formato taller interactivo :
      
      ¿ que no pudiste participar en vivo y en directo ?...:
acá [la disfrutas en diferido acá](https://fediverse.tv/w/pCFpsxXmW6ZHiiQnnXwSTu)
	  
## Pasado encuentro/taller online  (video / grabación):  "Emacs es tu amigo a la hora de programagar ~ IDE: Eglot, LSP y más..."

### ¿Cuándo fue? ¿ Quedó grabado y publicado ?

El domingo 10 de Noviembre de 2024, 18h CET - 12h en Mexico DF .

Si, acá puedes disfrutar [en diferido del taller encuentro gracias a la grabación, publicada en nuestro canal comunitario en Fediverse.tv]( )

### ¿Cuál es la propuesta ?: "Tomando Notas en Emacs: con Denote y Howm" 

- Acá [el enlace al sitio web de  "Denote"](https://protesilaos.com/emacs/denote)
	  
	  
---
## Pasado encuentro/taller online celebrado (videograbación):  "Tomando Notas en Emacs"

### ¿Cuándo fue? ¿ Quedó grabado y publicado ?

El domingo 3 de Noviembre de 2024, 18h CET - 12h en Mexico DF .

Si, acá puedes disfrutar [en diferido del taller encuentro gracias a la grabación, publicada en nuestro canal comunitario en Fediverse.tv](https://fediverse.tv/w/cC3TkACLye6vzKDZFRGJ3o)

### ¿Cuál es la propuesta ?: "Tomando Notas en Emacs: con Denote y Howm" 

- Acá [el enlace al sitio web de  "Denote"](https://protesilaos.com/emacs/denote)
- Acá [el enlace al "blogpost de N. sobre howm"](https://notxor.nueva-actitud.org/2024/04/02/tomar-notas-con-howm-en-emacs.html)
- Acá [el enlace al sitio web de "howm"](https://kaorahi.github.io/howm/index.html)
- Acá [tutorial sobre "howm", en inglés](https://github.com/Emacs101/howm-manual/blob/main/Howm_tutorial_eng.pdf)

---

2023:

---

## Pasado encuentro/taller online celebrado (videograbación):  "Jugando con apps. de escritorio en Emacs"

### ¿Cuándo fue? ¿ Quedó grabado y publicado ?

El viernes 15 de Marzo de 2023, 18h CET - 12h en Mexico DF .

Si, acá puedes disfrutar [en diferido del taller encuentro gracias a la grabación, publicada en nuestro canal comunitario en Fediverse.tv](https://fediverse.tv/w/7FFGdNmzPP3vCyHj4BcNUa)

### ¿Cuál es la propuesta ? ... ¿ 'Aplicaciones Emacs' de Escritorio ?

¿por qué no?... ahá, el  [prolífico Andros F.](https://soy.andros.dev/) compartió pantalla entorno a la [creación de aplicaciones de escritorio](https://programadorwebvalencia.com/creating-desktop-applications-using-the-emacs-core/) aprovechando el 'core' de Emacs.  

¿Transgrediendo los límites del enfoque clásico de uso de Emacs? xD

Algunas pasos (y recursos relacionados) que se mencionan en el transcurso del taller  :

- Requisito: primero habrás de ... 

	* [¿ Compilar Emacs ?, al estilo Andros - blogpost :](https://programadorwebvalencia.com/compilar-emacs-para-ubuntu-o-elementaryos/)
	* [¿ Compilar Emacs ?, al estilo Notxor - blogpost:](https://notxor.nueva-actitud.org/2023/08/05/compilar-emacs-desde-el-codigo-fuente.html)
	* [¿ Compilar Emacs ?, al estilo Justo - videotutorial:](https://fediverse.tv/w/eiRUM32GbcRkWsdGyUZXAV)

- Apunte : ¿Configurar 'tu propio arranque/inicio de Emacs para tu aplicación ?: `lisp/site-init.el` (también el `site-load.el` viene en la documentación Elisp ) ...

- App. de escritorio ~ Emacs ~  código fuente publicado :

    *  https://github.com/tanrax/lirve.el

Gracias, gracias, gracias Andros

"Happy (Emacs) Hacking"


> NOTA : ¿Quieres Participar?  en [esta libreta de notas compartida](https://pad.elbinario.net/p/hispa-emacs) puedes ver los detalles que anticipan las sucesivas sesiones Hispa-Emacs, en diferentes 'puntos' verás anotadas las diferentes propuestas, en orden cronológico. Haz tu propuesta !


***
***
***

---
## Taller online celebrado:  "Jugando con Bases de Datos (SQLite)" - (Videograbación)


#### ¿Cuándo fue ? 

El sábado Noviembre de 2023, 19h CET - online 

### ¿Qué ocurrió ?

Conversamos con Notxor, prolífico autor entorno (entre otras muchas cosas) a Emacs. 
- [Acá su Blog]() : fuente de inspiración para muchos.
- Y acá sus [contribuciones en forma de repositorios git](https://codeberg.org/Notxor) 

Xenodium se sumó al [final de la fiesta compartiendo 'Live' su sal y pimienta :-)](https://github.com/xenodium/dotsies/blob/main/emacs/ar/sqlite-mode-extras.el)

... el resultado [de remixar todo ello (ver la videograbación,en diferido)](https://fediverse.tv/w/bCw6xPvs1n8c68jKqFBEgW), con música GNU #Emacs fue un encuentro/taller lleno de aprendizajes.

"Happy (Emacs) Hacking"


***
***
***

---

## A vueltas con ... ¿Emacs y bases de datos SQLite ? ~ Conversando con Xenodium


Xenodium y su Arte ya nos habían visitado antes [(puedes ver el anterior encuentro/taller, acá)](https://fediverse.tv/w/iKJq93fRo6G5EtEdnfsobp). Fue una sesión llena de aprendizajes.

Conversaremos de nuevo con él entorno a sus ultimas contribuciones a Emacs, que esperamos tener ocasión de ir desgranando en sucesivas sesiones:
- [¿Sabías que ya puedes usar bases de datos (SQLite), en tu editor ?](https://xenodium.com/emacs-29s-sqlite-mode/)
- [Acá, por ejemplo, X. habla de Org-mode, programación literaria  y chatGPT](https://xenodium.com/noweb-more-glue-for-your-org-toolbox/) 
- Acá  escribe entorno [al paquete Elisp que ha creado para facilitar el uso de chatGPT en la Shell de Emacs](https://xenodium.com/a-chatgpt-emacs-shell)
- y mucho más...!

 ¿ Te lo perdiste ? ... [disfruta de este encuentro/taller lleno de aprendizajes, en diferido, en nuestro canal de Video ! ](https://fediverse.tv/w/bCw6xPvs1n8c68jKqFBEgW)


***
***
***

---
## Taller online celebrado:  "Notxor tiene un (Emacs Lisp) Blog" - (Videograbación)

#### ¿Cuándo fue ? 

El sábado 7 de Octubre de 2023, 19h CET - online 

### ¿Qué ocurrió ?

Conversaremos con Notxor, prolífico autor entorno (entre otras muchas cosas) a Emacs. 
- [Acá su Blog]() : fuente de inspiración para muchos.
- Y acá sus [contribuciones en forma de repositorios git](https://codeberg.org/Notxor) 


Últimamente nos hemos fijado en su incursión y experiencia en la [lengua madre de Emacs, Lisp. ](https://notxor.nueva-actitud.org/2023/08/12/lisp-y-scheme-embebidos.html)

Notxor iene tanto que contarnos ... Hemos empezado viendo como desarrolla su propio Blog, con OrgMode...Elisp, etc. Una maravilla. 

### Videograbación (diferido) :

Si te perdiste el taller en vivo y en directo, [siempre puedes disfrutarlo acá](https://fediverse.tv/w/cceMJ7ftkS4ANCNLkTi3Cf)

### Agradecimientos :

- A Notxor, por compartir su arte

- a  la Oficina del Software Libre de la Universidad de Zaragoza , ofreciendo su sala de video BigBlueButton de cortesía
- al colectivo detras de Fediverse.TV , [que alberga nuestro canal ](https://fediverse.tv/c/hispa_emacs/videos)

* * * 
* * *
* * * 

---
## Emacs en nuestro día a día.. v0.2 | Lisp y Eshell

[JAMSession Emacs improvisada... que ha quedado grabada aquí.](https://fediverse.tv/w/txWDUD8BkhTUHa3xjfU4Dx)
Un vistazo general y primeros pasos al uso cotidiano del lenguaje de [programación Lisp](https://es.wikipedia.org/wiki/Lisp) y su potente combinación con la Shell Emacs ( 'eshell' vs 'shell').

* * * 
* * *
* * * 

---
## Emacs en nuestro día a día.. v0.1 | JAMSession con música GNU :-)

[JAMSession Emacs improvisada... que ha quedado grabada aquí.](https://fediverse.tv/w/h9VxpoaBTxAQYKKG8KmeS3)
Un vistazo general al uso cotidiano de GNU Emacs, orientado a la escritura y el desarrollo Web.

De ello podría derivar, a modo indicativo, ¿ un primer "listado - borrador" propuesto ? ...para futuras sesiones a proponer de cara al otoño 2023 ?


Participa !

Encuentranos en :

    https://hispa-emacs.org
    IRC - chat : #emacs-es en Libera.chat
    XMPP - chat :  xmpp:hispa-emacs@salas.xmpp.elbinario.net?join
    Telegram : grupo 'EmacsOrgmode'



* * * 
* * *
* * * 

---
## Emacs, 'DWIM-shell-command '  (Do What I Mean - Haz Lo Que Te Digo)  ) - Videograbación del encuentro/taller (diferido) 


En esta ocasión vimos juntos una demostración del paquete 'DWIM-shell-command ' desarrollado por Xenodium !

Así quedó demostrado que combinando el [Dired Mode](https://www.emacswiki.org/emacs/DiredMode) y las [instrucciones desde la Shell en Emacs](https://www.gnu.org/software/emacs/manual/html_node/emacs/Shell.html)
podemos hacer del uso cotidiano de la herramienta algo más adaptado a nuestras necesidades. Un posible ejemplo es el el paquete (disponible en los repositorios MELPA) *Emacs DWIM*. Xenodium, su autor nos habla de ello [en este](https://xenodium.com/emacs-dwim-do-what-i-mean/) y en [este otro blogpost](https://xenodium.com/emacs-dwim-shell-command/).

Gracias, gracias, gracias Xenodium !!!

El encuentro/charla/taller quedó [acá videograbado](https://fediverse.tv/w/iKJq93fRo6G5EtEdnfsobp) para que lo disfrutes en diferido si es que no pudiste participar en vivo y en directo !? 


* * * 
* * *
* * * 


### aquí [tus propuestas para futuras charlas y/o cuestiones a plantear en la charlas ya programadas](https://pad.elbinario.net/p/hispa-emacs)  ?

---
###  Reporte del pasado encuentro online del día 06-01-2023 

quedó [acá videograbado](https://fediverse.tv/w/2HJdDEvhU4XueG9AXGzBxj) para que lo disfrutes en diferido si es que no pudiste participar en vivo y en directo !? 

( participa en el desarrollo y [preparación de futuros en encuentros en el Pad abierto](https://pad.elbinario.net/p/hispa-emacs) o en nuestros canales de chat)

Tuvo lugar en formato taller y en él tratamos de responder entre todos a las preguntas  .. ?  

*** Que ventajas tiene usar distribucion Emacs (como Lloica, Doom Emacs , Spacemacs, emacs-base,  etc..  ) ?
  * Vimos una demostración de Doom Emacs
  * jugoso debate al respecto...
  * discusión en relación al uso para edición y trabajo colaborativo simultáneo en Emacs ?!


Hablamos de GNU Emacs y de... , entre otras tantas cosas que se quedaron en el tintero !  Como por.ej.:

***    que es Bash ? que es Fish ? que es Zsh ? que diferencias hay ?

***    que es una terminal ? que es un emulador de terminal ? que diferencias hay ?

***    que terminal usar para trabajar con Bash desde Emacs ?

***    que alternativas tenemos  ?

*** ...


---

2022:

---

## Sábado 15 de de Octubre , 2022 - a las 18h hablamos de - <<'Lloica' una distribución Emacs >> (Videograbación)

![Lloica logo](img/lloica.svg)

Acá puedes ya [disfrutar de la grabación del maravilloso taller](https://fediverse.tv/w/wt8Vx1uKik7mFqvBCw7B2D) !.

Y acá [puedes acceder al repositorio](https://gitlab.com/pineiden/lloica-emacs) del proyecto desarrollado por David Pineiden, quien generosamente nos habló en esta ocasión sobre su experiencia en la elaboración de una distribución Emacs.


</div>
<div class="medium-4 columns">

---

### Acerca de Hispa Emacs
Somos entusiastas de Emacs en particular (y del mundo GNU+Linux en general) en el ámbito de habla hispana: nos encontramos cada primer viernes de mes.
La mejor forma de permanecer al día sobre las actividades via nuestras herramientas de chat.
Te apetece contactar con nosotros? ... acercate, presentate y súmate a la conversación !

### IRC #emacs-es
[#emacs-es](irc://chat.libera.chat/emacs-es) (en Libera.Chat) -->

## XMPP - sala/grupo chat :
[xmpp:hispa-emacs@ =>](xmpp:hispa-emacs@salas.xmpp.elbinario.net?join)

#### Grupo Telegram
[EmacsOrgmode en ES](https://t.me/emacsorgmode)

### En el Fediverso (Universo Federado)

#### Canal de Video
[En la TV hispana del Fediverso](https://fediverse.tv/a/emacs_org/)

#### Microblogging federado 
[Cuenta en un nodo Mastodon](https://fosstodon.org/@hispaemacs)

<!-- En la cuenta Twitter:
[@emacs-es](https://twitter.com/emacs-es) -->

<!-- And there's a [YouTube Channel](https://www.youtube.com/channel/UC1O8700SW-wuC4fvDEoGzOw) <span class='text-cursor'>&nbsp;</span> -->

<!-- Contacto via email: [organizers email][contact] -->



### Próximos encuentros

* viernes   7 de Marzo de 2025 
* sábado    5 de Abril de 2025 
* viernes   7 de Mayo de 2025 



### Archivo

[Archivo de encuentros pasados](/archive.html)

</div></div></section>

<section id="end-fold"><div class="row"><div class="large-12 columns">

## Garantía novata - acogida

Si te apetece una introducción básica a Emacs, o te estás empezando 
a hacer preguntas estaremos más que encantados de poder acompañarte en ello.
Basta con que te acerques a cualquiera de nuestras actividades y 
canales de comunicación.


## Charlas / Talleres

La charlas suelen tener una duración de 20 a 45 minutillos + 5 min. para abrir el juego a preguntas (o viceversa), y suelen ser entorno a Emacs y Orgmode) .
Las charlas relámpago son de 5 minutos + 5 minutes para abrir el juego a preguntas, entorno a cualquier cuestión (p.ej. GNU + Linux , Bash... y no sólo!) 
La única 'regla' sugerida "brevedad y estímulo" + cultura copyleft .

En estos momentos la mejor forma de proponer charlas / talleres  es a través de alguno de nuestros canales / herramientas chat. Anímat a participar :-)



<!-- ## Format -->

<!-- We try to keep the structure pretty loose, we are not the typical -->
<!-- "three half hour talks" type of user group. There are often small -->
<!-- impromptu presentations or demos. For the rest people just hack -->
<!-- together on whatever they find interesting, help each other to make -->
<!-- their Emacs setups even more awesome, or chat about whatever they've -->
<!-- been up to, Emacs related, or not. -->

<!-- [GitHub Issues][ghi] and the [mailing list][ml] can be used to propose -->
<!-- topics. -->

<!-- ## Miembros | ej. de emacs-berlin -->

<!-- People that show up to our meetings regularly or on occasion. Links go -->
<!-- to their emacs config. -->

<!-- * [plexus](https://github.com/plexus/.emacs.d) -->
<!-- * [pxlpnk](https://github.com/pxlpnk/emacs.d) -->
<!-- * [febeling](https://github.com/febeling/emacsd) -->
<!-- * [Jano](https://github.com/janogonzalez/.emacs.d) -->
<!-- * [Stefan](https://www.skamphausen.de/cgi-bin/ska/My_Configuration_of_Emacsen) -->
<!-- * [Diez](https://bitbucket.org/deets/emacs-git) -->
<!-- * [Jack](https://github.com/jackrusher/dotemacs) -->
<!-- * [til](https://tils.net/init.el.txt) -->
<!-- * [Peter](https://github.com/ptrv/emacs.d) -->
<!-- * [Andreas](https://github.com/andreas-roehler/werkstatt) -->

</div></div></section>

<!--
[contact]: mailto:emacs@librebits.net "Send an email to organizers"
[ghi]: https://github.com/emacs-berlin/emacs-berlin.org/issues "emacs-berlin GitHub Issues"
-->
