---
title: (emacs-berlin 'archive')
layout: default
---
{::options parse_block_html="true" /}

<section id="above-fold"><div class="row"><div class="large-12 columns intro-info">

{:#emacs-berlin-lockup width="300px"}
![emacs-berlin logo](img/hispa-emacs.org.png)

</div></div></section>

<section id="below-fold"><div class="row"><div class="medium-8 columns">
# Archivo

No todo encuentro deriva en una página de _archivo_ , tan sólo cuando alguien la genera proactivemente 
(['Pull request' en el repositorio son más que bienvenidos](https://gitlab.com/emacsorg/hispa-emacs/)).

## Encuentros Previos (de la vecina Emacs-Berlín, como muestra del _'Archivado'_ )
* [February 2020](20200226-notes.html)
* Noviembre 2019: Intro to elisp with a quick link-to-GitHub tool by Daniel, then watched [Emacs: The Editor for the Next Forty Years](https://media.emacsconf.org/2019/26.html) suggested by Ilia
* November 2018: [Emacs, India and Geminis](https://worldgeek.org/gemini-in-action-india-2018/gemini-in-action.html) by [Gary](https://worldgeek.org/)
* September 2018: [Thread-safe Tramp](thread-safe-tramp-2018-09.html) by Michael Albinus ([org-file](thread-safe-tramp-2018-09.org))




</div></div></section>
